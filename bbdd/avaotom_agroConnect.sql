-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-04-2020 a las 19:37:22
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `avaotom_agroConnect`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `mac` varchar(30) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_parcel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `devices`
--

INSERT INTO `devices` (`id`, `mac`, `id_location`, `id_parcel`) VALUES
(4, '00:1B:44:11:3A:B7', 1, 1),
(5, '00:1B:44:11:3A:B8', 2, 1),
(6, '00:1B:44:11:3A:B9', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enterprises`
--

CREATE TABLE `enterprises` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cif` varchar(10) NOT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `enterprises`
--

INSERT INTO `enterprises` (`id`, `name`, `cif`, `city`, `address`) VALUES
(1, 'AgroConnect', '1234567890', 'Ontinyent', 'Calle Falsa 123'),
(2, 'AgroDisconnect', '0987654321', 'Gandia', 'Calle Verdadera 123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `humidity`
--

CREATE TABLE `humidity` (
  `id` int(11) NOT NULL,
  `value` float NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `humidity`
--

INSERT INTO `humidity` (`id`, `value`, `id_location`, `date`) VALUES
(1, 34, 1, '2020-03-29'),
(2, 12, 1, '2020-03-30'),
(3, 56, 1, '2020-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `location`
--

INSERT INTO `location` (`id`, `lat`, `lng`) VALUES
(1, 38.9952, -0.178163),
(2, 38.9935, -0.177176),
(3, 38.9941, -0.172048);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `luminosity`
--

CREATE TABLE `luminosity` (
  `id` int(11) NOT NULL,
  `value` float NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `luminosity`
--

INSERT INTO `luminosity` (`id`, `value`, `id_location`, `date`) VALUES
(1, 50, 1, '2020-03-29'),
(2, 55, 1, '2020-03-30'),
(3, 60, 1, '2020-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parcels`
--

CREATE TABLE `parcels` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `id_enterprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `parcels`
--

INSERT INTO `parcels` (`id`, `name`, `description`, `id_enterprise`) VALUES
(1, 'Los manzanos', 'Un campo de manzanos', 1),
(2, 'Los sandíos', NULL, 1),
(3, 'Los perales', 'Un campo de peras', 2),
(4, 'Los melonares', 'Un campo de melones', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salinity`
--

CREATE TABLE `salinity` (
  `id` int(11) NOT NULL,
  `value` float NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `salinity`
--

INSERT INTO `salinity` (`id`, `value`, `id_location`, `date`) VALUES
(1, 43, 1, '2020-03-29'),
(2, 32, 1, '2020-03-30'),
(3, 10, 1, '2020-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperature`
--

CREATE TABLE `temperature` (
  `id` int(11) NOT NULL,
  `value` float NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `temperature`
--

INSERT INTO `temperature` (`id`, `value`, `id_location`, `date`) VALUES
(1, 20, 1, '2020-03-29'),
(2, 21, 1, '2020-03-30'),
(3, 11, 1, '2020-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(60) DEFAULT NULL,
  `role` enum('root','sat','admin','user') NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `id_enterprise` int(11) DEFAULT NULL,
  `token` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `role`, `email`, `password`, `telephone`, `id_enterprise`, `token`) VALUES
(1, 'jogilo', NULL, 'user', 'jogilo@jogilo.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL),
(2, 'dapaso', NULL, 'user', 'dapaso@dapaso.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL),
(3, 'dani', NULL, 'admin', 'dani@dani.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 2, NULL),
(4, 'victor', NULL, 'admin', 'victor@victor.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 1, NULL),
(5, 'juan', NULL, 'sat', 'juan@sat.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL),
(6, 'nacho', NULL, 'sat', 'nacho@sat.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL),
(7, 'alejandro', NULL, 'root', 'avato@root.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL),
(8, 'dios', NULL, 'root', 'dios@root.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vertices`
--

CREATE TABLE `vertices` (
  `id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `id_parcel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vertices`
--

INSERT INTO `vertices` (`id`, `lat`, `lng`, `id_parcel`) VALUES
(1, 38.9957, -0.178843, 1),
(2, 38.9956, -0.167091, 1),
(3, 38.9936, -0.167563, 1),
(4, 38.9931, -0.177802, 1),
(5, 38.9935, -0.177827, 1),
(7, 39.0308, -0.237179, 2),
(8, 39.0307, -0.235453, 2),
(9, 39.0316, -0.234247, 2),
(10, 39.0316, -0.232955, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mac` (`mac`),
  ADD KEY `fk_devices_location` (`id_location`),
  ADD KEY `fk_devices_parcels` (`id_parcel`);

--
-- Indices de la tabla `enterprises`
--
ALTER TABLE `enterprises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`cif`);

--
-- Indices de la tabla `humidity`
--
ALTER TABLE `humidity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_location_humidity` (`id_location`);

--
-- Indices de la tabla `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `luminosity`
--
ALTER TABLE `luminosity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_location_luminosity` (`id_location`);

--
-- Indices de la tabla `parcels`
--
ALTER TABLE `parcels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_parcels_enterprise` (`id_enterprise`) USING BTREE;

--
-- Indices de la tabla `salinity`
--
ALTER TABLE `salinity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_location_salinity` (`id_location`);

--
-- Indices de la tabla `temperature`
--
ALTER TABLE `temperature`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_location_temperature` (`id_location`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `fk_users_enterprise` (`id_enterprise`);

--
-- Indices de la tabla `vertices`
--
ALTER TABLE `vertices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vertices_parcels` (`id_parcel`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `enterprises`
--
ALTER TABLE `enterprises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `humidity`
--
ALTER TABLE `humidity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `luminosity`
--
ALTER TABLE `luminosity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `parcels`
--
ALTER TABLE `parcels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `salinity`
--
ALTER TABLE `salinity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `temperature`
--
ALTER TABLE `temperature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `vertices`
--
ALTER TABLE `vertices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `fk_devices_location` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_devices_parcels` FOREIGN KEY (`id_parcel`) REFERENCES `parcels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `humidity`
--
ALTER TABLE `humidity`
  ADD CONSTRAINT `fk_location_humidity` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `luminosity`
--
ALTER TABLE `luminosity`
  ADD CONSTRAINT `fk_location_luminosity` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `parcels`
--
ALTER TABLE `parcels`
  ADD CONSTRAINT `fk_parcels_enterprises` FOREIGN KEY (`id_enterprise`) REFERENCES `enterprises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `salinity`
--
ALTER TABLE `salinity`
  ADD CONSTRAINT `fk_location_salinity` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `temperature`
--
ALTER TABLE `temperature`
  ADD CONSTRAINT `fk_location_temperature` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_enterprise` FOREIGN KEY (`id_enterprise`) REFERENCES `enterprises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vertices`
--
ALTER TABLE `vertices`
  ADD CONSTRAINT `fk_vertices_parcels` FOREIGN KEY (`id_parcel`) REFERENCES `parcels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
