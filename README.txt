=============================================================================================================================
AggroConnect-PW-2020-G12 es un proyecto realizado en la Universidad Politecnica de Valencia, 
por el grupo numero doce del grado de Tecnologias Interactivas. 

Nuestra misión es recoger los datos de unas sondas que hemos desarrollado en el primer 
cuatrimestre y mostrarlos en una web atractiva, para que las personas que trabajen en 
campo puedan observar en todo momento datos con información de sus parcelas en tiempo real.

Nuestro grupo de trabajo, esta conformado por Alejandro Vañó, Daniel Poquet, Nacho Sandalinas, Víctor García y Juan Ferrera.
=============================================================================================================================