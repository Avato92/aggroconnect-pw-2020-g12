<?php
$id = $_GET['parcel_id'];

$sqlVertices = "SELECT * FROM vertices WHERE `id_parcel` = '$id'";
$resVertices = mysqli_query($conn, $sqlVertices);

$arrayVerticesParcela = array();
while($fila = mysqli_fetch_assoc($resVertices)){
    $vertice = array(
        "lat" => (float)$fila["lat"],
        "lng" => (float)$fila['lng']
    );
    array_push($arrayVerticesParcela, $vertice);
}

// Inner join para relacionar mediante la clave foranea "id_parcel" las dos tablas
$sqlDispositivosYlocation = "SELECT * FROM devices INNER JOIN location ON devices.id_location = location.id WHERE devices.id_parcel='$id'";
$resDispositivosYlocation = mysqli_query($conn, $sqlDispositivosYlocation);

$arrayDispositivosYlocation = array();
while($fila = mysqli_fetch_assoc($resDispositivosYlocation)){
    print_r($fila);
    $dispositivoYlocation = array(
        "id" => $fila["id"],
        "id_location" => $fila['id_location'],
        "mac" => $fila["mac"],
        "lat" => (float)$fila["lat"],
        "lng" => (float)$fila["lng"]
    );
    array_push($arrayDispositivosYlocation, $dispositivoYlocation);
}

if (empty($arrayVerticesParcela) || empty($arrayDispositivosYlocation)){
// Si la consulta no devuelve información sobre los vertices de la parcela o sus dispositivos devuelvo un código HTTP 401
    $http_code = 404;
}else {
// Si he obtenido correctamente los datos de la consulta devuelvo un código HTTP 200
    $salida = array(
        "vertices" => $arrayVerticesParcela,
        "dispositivos" => $arrayDispositivosYlocation
    );
    $http_code = 200;
}