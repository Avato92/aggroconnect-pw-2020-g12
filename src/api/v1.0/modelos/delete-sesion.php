<?php
session_start();
if(isset($_SESSION['userInfo'])){
    session_destroy();
    $http_code = 200;
} else {
    $http_code = 401;
}