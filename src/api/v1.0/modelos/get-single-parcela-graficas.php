<?php
$locationId = $_GET['location_id'];

$sqlSalinity = "SELECT * FROM salinity WHERE salinity.id_location='$locationId'";
$resSalinity = mysqli_query($conn, $sqlSalinity);
$arraySalinity = array();
while($fila = mysqli_fetch_assoc($resSalinity)){
    array_push($arraySalinity, $fila);
}

$sqlTemperature = "SELECT * FROM temperature WHERE temperature.id_location='$locationId'";
$resTemperature = mysqli_query($conn, $sqlTemperature);
$arrayTemperature = array();
while($fila = mysqli_fetch_assoc($resTemperature)){
    array_push($arrayTemperature, $fila);
}

$sqlLuminosity = "SELECT * FROM luminosity WHERE luminosity.id_location='$locationId'";
$resLuminosity = mysqli_query($conn, $sqlLuminosity);
$arrayLuminosity = array();
while($fila = mysqli_fetch_assoc($resLuminosity)){
    array_push($arrayLuminosity, $fila);
}

$sqlHumidity = "SELECT * FROM humidity WHERE humidity.id_location='$locationId'";
$resHumidity = mysqli_query($conn, $sqlHumidity);
$arrayHumidity = array();
while($fila = mysqli_fetch_assoc($resHumidity)){
    array_push($arrayHumidity, $fila);
}

// Si no se han obtenido datos de alguna tabla devuelvo un código HTTP 401
if(empty($arrayHumidity) || empty($arrayTemperature || empty($arrayLuminosity) || $arrayHumidity)){
    $http_code = 401;
}else {
// Si se han obtenido los datos exitosamente devuelvo el código HTTP 200
    $http_code = 200;
    $salida = array (
        "salinity" => $arraySalinity,
        "temperature" => $arrayTemperature,
        "luminosity" => $arrayLuminosity,
        "humidity" => $arrayHumidity
    );
}

