<?php
// Si el método es POST
$idParcel = $_POST['id-parcela'];
$parcelName = $_POST['parcel-name'];
$idEmpresa = $_POST['id-empresa'];
$description = $_POST['description'];
$arrayPerimetro = json_decode($_POST['perimetroParcelaJSON'],true);
$arrayDispositivos = json_decode($_POST['dispositivosParcelaJSON'],true);
//var_dump($arrayDispositivos);
//Si no recibo por $_POST el campo con la id de la empresa y su nombre devuelvo error http 400
//Description es opcional
if (!isset($idEmpresa) || $idEmpresa == '' || !isset($parcelName) || $parcelName == ''){
    $http_code = 400;
}else {
//Compruebo que existe una empresa con la id introducida
    $sqlCheckIdEmpresa = "SELECT * FROM enterprises WHERE `id` = '$idEmpresa'";
    $resCheckIdEmpresa = mysqli_query($conn, $sqlCheckIdEmpresa);
    $resArrayCheckIdEmpresa = mysqli_fetch_array($resCheckIdEmpresa);

//Si no hay ninguna empresa devuelvo un código 401
    if (!isset($resArrayCheckIdEmpresa)){
        $http_code = 401;
    }else {
//Compruebo que no haya ninguna parcela de la empresa con el nombre de la nueva parcela que queremos editar
        $sqlCheckName = "SELECT * FROM parcels WHERE `id_enterprise` = '$idEmpresa' AND `name`= '$parcelName'";
        $resCheckName = mysqli_query($conn, $sqlCheckName);
        $resArrayCheckName = mysqli_fetch_array($resCheckName);
        if (isset($resArrayCheckName)) {
            $http_code = 402;
        }else {
            //En caso contrario procedo a editar los datos validados en la bbdd
            //Edito la tupla correspondiente a la parcela determinada
            $sql = "UPDATE parcels SET name = '$parcelName', description = '$description', id_enterprise ='$idEmpresa' WHERE id = $idParcel";
            $res = mysqli_query($conn, $sql);

            //Antes de insertar los datos de vertices y dispositivos borro los datos que tenía posteriormente almacenados
            $sqlDeleteDevices = "DELETE FROM devices WHERE `id_parcel` = '$idParcel'";
            $res = mysqli_query($conn, $sqlDeleteDevices);

            //Falta eliminar sus location relacionadas, debería de hacerse automaticamente DELETE ON CASCADE
            $sqlDeleteDevices = "DELETE FROM vertices WHERE `id_parcel` = '$idParcel'";
            $res = mysqli_query($conn, $sqlDeleteDevices);

            //Añado todas las posiciones del perimetro seleccionadas por el usuario a la tabla de vertices (de la parcela)
            foreach ($arrayPerimetro as $latLongVertice){
                $lat = $latLongVertice['lat'];
                $long = $latLongVertice['lng'];
                $sqlInsertVertice = "INSERT INTO vertices (lat,lng,id_parcel) VALUES ($lat,$long,$idParcel)";
                mysqli_query($conn, $sqlInsertVertice);
            }

            //*mysqliquery devuelve false si hay un error, tengo que comprobar que si alguna query falla se deshagan los inserts anteriores*
            foreach ($arrayDispositivos as $latLongDevice){
                $lat = $latLongDevice['lat'];
                $long = $latLongDevice['lng'];
                $mac = $latLongDevice['mac'];

                //Añado las localizaciones de los dispositivos a la bbdd
                $sqlInsertLocation = "INSERT INTO location (lat,lng) VALUES ($lat,$long)";
                $resInsertLocation = mysqli_query($conn, $sqlInsertLocation);

                //Añado los dispositivos a la bbdd
                $idLocationLastInsert = mysqli_insert_id($conn);
                $sqlInsertDevice = "INSERT INTO devices (mac,id_location,id_parcel) VALUES ('$mac',$idLocationLastInsert,$idParcel)";
                $resInsertDevice = mysqli_query($conn, $sqlInsertDevice);
            }


            $http_code = 200;
        }
    }

}