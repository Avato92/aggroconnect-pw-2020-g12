<?php

$parcelName = $_POST['parcel-name'];
$idParcela = $_POST['id-parcela'];

//Si no recibo por $_POST el campo con la id de la empresa y su nombre devuelvo error http 400
if (!isset($idParcela) || $idParcela == ''){
    $http_code = 400;
}else {
//Compruebo que existe una parcela con la id introducida
    $sqlCheckIdParcela = "SELECT * FROM parcels WHERE `id` = '$idParcela'";
    $resCheckIdParcela = mysqli_query($conn, $sqlCheckIdParcela);
    $resArrayCheckIdParcela = mysqli_fetch_array($resCheckIdParcela);
//Si no hay ninguna parcela con esa id devuelvo un código 404
    if (!isset($resArrayCheckIdParcela)){
        $http_code = 404;
    }else {
//En caso contrario procedo a hacer un delete de la parcela en la bbdd
        $sql = "DELETE FROM parcels WHERE `id` = '$idParcela'";
        $res = mysqli_query($conn, $sql);
        $http_code = 200;
    }
}
