<?php

// Si el método es PUT
if ($_POST['_method'] == 'put'){

    $parcelId = $_POST['parcel-id'];
    $parcelName = $_POST['parcel-name'];
    $idEmpresa = $_POST['id-empresa'];
    $description = $_POST['description'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];

//Si no recibo por $_POST el campo con la id de la empresa y su nombre devuelvo error http 400
    if (!isset($parcelId) || $parcelId == '' || !isset($parcelName) || $parcelName == '' || !isset($idEmpresa) || $idEmpresa == '' || !isset($description) ||
        !isset($lat) || $lat == '' || !isset($long) || $long == ''){
        $http_code = 400;
    }else {
//Compruebo que existe una empresa con la id introducida
        $sqlCheckIdEmpresa = "SELECT * FROM enterprises WHERE `id` = '$idEmpresa'";
        $resCheckIdEmpresa = mysqli_query($conn, $sqlCheckIdEmpresa);
        $resArrayCheckIdEmpresa = mysqli_fetch_array($resCheckIdEmpresa);
//Si no hay ninguna empresa devuelvo un código 401
        if (!isset($resArrayCheckIdEmpresa)){
            $http_code = 401;
        }else {
//Compruebo que no haya ninguna parcela de la empresa con el nombre que queremos asignarle
            $sqlCheckName = "SELECT * FROM parcels WHERE `id_enterprise` = '$idEmpresa' AND `name`= '$parcelName'";
            $resCheckName = mysqli_query($conn, $sqlCheckName);
            $resArrayCheckName = mysqli_fetch_array($resCheckName);
            if (isset($resArrayCheckName)) {
                $http_code = 402;
            } else {
//En caso contrario procedo a hacer un update de los datos validados en la bbdd
                $sql = "UPDATE parcels SET name='$parcelName', id_enterprise = $idEmpresa, description='$description' WHERE `id`=$parcelId";
                $res = mysqli_query($conn, $sql);

                $sqlVertice = "UPDATE vertices SET lat='$lat', long = $long, id_parcel=$parcelId WHERE `id_parcel`=$parcelId";
                $resVertice = mysqli_query($conn, $sqlVertice);

                $http_code = 200;
            }
        }
    }
}else if($_POST['_method'] == 'delete') {
// Si el método es DELETE
    $idParcela = $_POST['id-parcela'];

//Si no recibo por $_POST el campo con la id de la empresa y su nombre devuelvo error http 400
    if (!isset($idParcela) || $idParcela == ''){
        $http_code = 400;
    }else {
//Compruebo que existe una parcela con la id introducida
        $sqlCheckIdParcela = "SELECT * FROM parcels WHERE `id` = '$idParcela'";
        $resCheckIdParcela = mysqli_query($conn, $sqlCheckIdParcela);
        $resArrayCheckIdParcela = mysqli_fetch_array($resCheckIdParcela);
//Si no hay ninguna parcela con esa id devuelvo un código 404
        if (!isset($resArrayCheckIdParcela)){
            $http_code = 404;
        }else {
//En caso contrario procedo a hacer un delete de la parcela en la bbdd
            $sql = "DELETE FROM parcels WHERE `id` = '$idParcela'";
            $res = mysqli_query($conn, $sql);
            $http_code = 200;
        }
    }

}else {
// Si el método es POST
    $parcelName = $_POST['parcel-name'];
    $idEmpresa = $_POST['id-empresa'];
    $description = $_POST['description'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];

//Si no recibo por $_POST el campo con la id de la empresa y su nombre devuelvo error http 400
//Description es opcional
    if (!isset($idEmpresa) || $idEmpresa == '' || !isset($parcelName) || $parcelName == ''){
        $http_code = 400;
    }else {
//Compruebo que existe una empresa con la id introducida
        $sqlCheckIdEmpresa = "SELECT * FROM enterprises WHERE `id` = '$idEmpresa'";
        $resCheckIdEmpresa = mysqli_query($conn, $sqlCheckIdEmpresa);
        $resArrayCheckIdEmpresa = mysqli_fetch_array($resCheckIdEmpresa);

//Si no hay ninguna empresa devuelvo un código 401
        if (!isset($resArrayCheckIdEmpresa)){
            $http_code = 401;
        }else {
//Compruebo que no haya ninguna parcela de la empresa con el nombre de la nueva parcela que queremos crear
            $sqlCheckName = "SELECT * FROM parcels WHERE `id_enterprise` = '$idEmpresa' AND `name`= '$parcelName'";
            $resCheckName = mysqli_query($conn, $sqlCheckName);
            $resArrayCheckName = mysqli_fetch_array($resCheckName);
            if (isset($resArrayCheckName)) {
                $http_code = 402;
            }else {
//En caso contrario procedo a introducir los datos validados en la bbdd
                $sql = "INSERT INTO parcels (name,description,id_enterprise) VALUES ('$parcelName','$description',$idEmpresa)";
                $res = mysqli_query($conn, $sql);

                $sqlGetParcela = "SELECT * FROM parcels WHERE `id_enterprise` = '$idEmpresa' AND `name`= '$parcelName'";
                $resGetParcela = mysqli_query($conn, $sqlGetParcela);
                $idParcel = mysqli_fetch_array($resGetParcela)['id'];

//por ahora la parcela tiene una posición única
                $sqlInsertVertice = "INSERT INTO vertices (lat,lng,id_parcel) VALUES ($lat,$long,$idParcel)";
                $resInsertVertice = mysqli_query($conn, $sqlInsertVertice);
                $http_code = 200;
            }
        }
    }
}

