<?php
    session_start();

    if(!isset($_SESSION['userInfo'])){

        /**
         * Usuario no logueado
         */

        $http_code = 400;

    }else{

        /**
         * Comprobamos que el usuario que ha accedido a dicha petición sea con un rol de root o sat
         */

        if($_SESSION['userInfo']['role'] == "root" || $_SESSION['userInfo']['role'] == "sat") {

            /**
             * Consulta SQL
             */

            $sql = "SELECT *
                            FROM `enterprises`";

            $res = mysqli_query($conn, $sql);

            /**
             * Comprobamos que la consulta aporte resultados
             */

            if(empty($res)){

                /**
                 * Error en caso de no obtener resultados (BBDD sin empresas)
                 */

                $http_code = 402;

            }else{

                /**
                 * Tenemos resultados
                 */

                $http_code = 200;

                /**
                 * Bucle para formatear los datos que vamos a devolver al frontend
                 */

                while($fila = mysqli_fetch_assoc($res)){
                    $enterprises = array(
                                    "id" => $fila['id'],
                                    "name" => $fila['name'],
                                    "cif" => $fila['cif'],
                                    "city" => $fila['city'],
                                    "address" => $fila['address']
                    );

                    /**
                     * Introducimos lo que queremos devolver en la variable que previamente hemos inicializado para devolver al frontend
                     */

                    array_push($salida, $enterprises);
                    
                }
                
            }
        }else{

            /**
             * Error, usuario no autorizado para realizar esta búsqueda
             */

            $http_code = 401;
            $salida = $_SESSION['userInfo'];
        }
    }

