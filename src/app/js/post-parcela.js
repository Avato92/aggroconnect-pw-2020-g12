///////////////////////////////////////////////////////////////////////////////////
// Fetch de todas las empresas
//

// Fetch de la de la tabla enterprises
fetch('../api/v1.0/enterprises', {
    method: "GET",
}).then(res => {
    return res.json();
}).then(function(response){
    let select = document.getElementById("id-empresa");

    //Dependiendo del role del usuario
    // -> el usuario tiene bloqueada la selección de empresa (usamos su id_empresa asociado)
    // -> muestro todas las empresas en un select
    if (response.role == "admin" || response.role == "user"){
        option.value = response.id_enterprise;
        select.add(option);
        //Escondo el select y su label
        document.getElementById("divSelect").classList.add("esconde");
    } else {
        console.log(response)
        response.forEach(function (enterprise) {
            let option = document.createElement("option");
            option.text = enterprise.name;
            option.value = enterprise.id;
            select.add(option);
        })
    }
});

                                                                                 //
                                                                                 //
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//
// Esta funcion carga la configuración del mapa api de google y funcionalidades js
//

function loadSettings() {

// Configuarción de inicialización del mapa

    var myLatlng = {lat: -25.363, lng: 131.044};

    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4, center: myLatlng, draggableCursor: 'default', draggingCursor: 'pointer'});


    // Creo la ventana inicial de información para que el usuario sepa que hacer
    var infoWindow = new google.maps.InfoWindow(
        {content: 'Clica en "situar perimetro" y dibuja el perimetro sobre este mapa, puedes añadir dispositivos ' +
                  'a la parcela haciendo click en "situar dispositivo" e introduciendo las coordenadas', position: myLatlng});
    infoWindow.open(map);

                                                                                 //
                                                                                 //
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
// En estas variables guardare los event listeners de los botones encargados de
// representar en el mapa el perimetro y los dispositivos
// los guardo para poder borrarlo (siguiendo la documentacion de la api de google)

    var listenerPerimetro;
    var listenerDispositivos;

    // Esta funcion es la encargada de borrar los eventListener sobre el mapa
    function clearMapEventListeners() {
        google.maps.event.removeListener(listenerPerimetro);
        google.maps.event.removeListener(listenerDispositivos);
    }
                                                                                 //
                                                                                 //
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//
// Añado la funcion de borrar los eventListener sobre el boton correspondiente
//
    var botonBorrar = document.getElementById("botonBorrar")
    botonBorrar.addEventListener("click", borrarPosicionesParcela)

    // En arrayPosicionesParcela guardare las posiciones del perimetro dibujadas sobre el mapa por el usuario
    // En arrayPosicionesDispositivos guardare las posiciones de los dispositivos dibujadas sobre el mapa por el usuario
    // En markers guardo todos los marcadores dibujados en el mapa (esto es necesario para poder borrarlos)
    var arrayPosicionesPerimetro = [];
    var arrayPosicionesDispositivos = [];
    var markers = [];

    //envio de información del formulario
    var form = document.getElementById("form");

    var submitButton = document.getElementById("botonSendForm");

    // Al clicar en el boton de submit se recoge la información del formulario y se añaden a este formulario
    // los datos de las posiciones de la parcela y sus dispositivos en formato JSON
    submitButton.addEventListener("click", function (event) {
        event.preventDefault();
        var formData = new FormData(form);
        formData.append('perimetroParcelaJSON', JSON.stringify( arrayPosicionesPerimetro ))
        formData.append('dispositivosParcelaJSON', JSON.stringify( arrayPosicionesDispositivos ))

        // Al producirse un submit envío los datos del formulario mediante un fetch
        fetch('../api/v1.0/parcela', {
            method: "POST",
            body: formData
        }).then(function(response) {
            if(response.status == 200){
                console.log(response)
                //location.href = "./list-parcels.html"
            }else if(response.status == 400){
                console.log(response.status)
            }
        }).then(function(texto) {
            console.log(texto);
        }).catch(function(err) {
            console.log(err);
       })});

    // Esta funcion es la encargada de borrar todas las localizaciones dibujadas en el mapa
    function borrarPosicionesParcela(){
        event.preventDefault()
        arrayPosicionesPerimetro = [];
        arrayPosicionesDispositivos = [];
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

///////////////////////////////////////////////////////////////////////////////////
//  addMarker recibirá como argumento el click del usuario sobre el mapa
//  situa un marcador sobre la localizacion seleccionada y dependiendo de que
//  opcion haya seleccionada el usuario guarda la posicion como
//  punto del perimetro o como localizacion de un dispositivo (respectivo array)

    function addMarkerDispositivos(mapsMouseEvent) {
        event.preventDefault()
        document.getElementById("botonPerimetro").classList.remove("borderBoton");
        document.getElementById("cuadroPerimetro").classList.remove("remarcaCuadro");
        document.getElementById("botonDispositivos").classList.add("borderBoton");

        var lat = prompt("Introduce la latitud del dispositivo");
        var long = prompt("Introduce la longitud del dispositivo");
        var mac = prompt("Introduce la dirección MAC del dispositivo");
        var latLong = {lat : parseFloat(lat), lng : parseFloat(long)};
        var positionJSON = {lat : parseFloat(lat), lng : parseFloat(long), mac : mac};
        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            title: 'Hello World!',
            icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
            }
        });
        markers.push(marker);
        arrayPosicionesDispositivos.push(positionJSON);
    }

    function addMarkerPerimetro(mapsMouseEvent) {
        var marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng.toJSON(),
            map: map,
            title: 'Hello World!'
        });
        markers.push(marker);
        arrayPosicionesPerimetro.push(mapsMouseEvent.latLng.toJSON());
    }
                                                                                 //
                                                                                 //
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
//  Dependiendo de que boton clique el usuario añado la funcionalidad de dibujar
//  puntos del perimetro o localizaciones de los dispositivos y
//  remarco el cuadro de color correspondiente
    
    var botonPerimetro = document.getElementById("botonPerimetro")
    botonPerimetro.addEventListener("click", onClickPerimetro)

    function onClickPerimetro(){
        event.preventDefault()
        //document.getElementById("cuadroDispositivos").classList.remove("remarcaCuadro");
        document.getElementById("cuadroPerimetro").classList.add("remarcaCuadro");
        document.getElementById("botonPerimetro").classList.add("borderBoton");
        document.getElementById("botonDispositivos").classList.remove("borderBoton");
        clearMapEventListeners()
        // Configure the click listener.
        listenerPerimetro = map.addListener('click', addMarkerPerimetro);
    }

    var botonDispositivos = document.getElementById("botonDispositivos")
    botonDispositivos.addEventListener("click", addMarkerDispositivos)

    function onClickDispositivos(){
        event.preventDefault()
        document.getElementById("cuadroPerimetro").classList.remove("remarcaCuadro");
        document.getElementById("cuadroDispositivos").classList.add("remarcaCuadro");
        clearMapEventListeners()
        // Configure the click listener.
        listenerDispositivos = map.addListener('click', addMarkerDispositivosOld);

    }

    // Con esta funcion cierro sesion y redirijo a la landing page
    function deleteSesionAndRedirectLanding(){
        event.preventDefault();
        fetch('../api/v1.0/sesion', {
            method: "DELETE",
        }).then(function (response){
            if (response.status == 200){
                location.href = "./../index.html"
            }
        })
    }
    // Añado el event listener para los botones de cerrar sesion (movil y desktop)
    document.getElementById("aCerrarSesion").addEventListener("click", deleteSesionAndRedirectLanding);
    document.getElementById("aCerrarSesionMovil").addEventListener("click", deleteSesionAndRedirectLanding);

    // Con esta funcion redirijo a list-parcels o list-enterprises dependiendo del role del usuario
    function volverAtras(){
        event.preventDefault();
        fetch('../api/v1.0/sesion', {
            method: "GET",
        }).then(res => {
            return res.json();
        }).then(function (userInfo) {
            if (userInfo.role == "admin" || userInfo.role == "user"){
                location.href = "./list-parcels.html"
            } else {
                location.href = "./list-enterprises.html"
            }
        })
    }
    // Añado el event listener para los botones de volver atras (movil y desktop)
    document.getElementById("aVolverAtras").addEventListener("click", volverAtras);
    document.getElementById("aVolverAtrasMovil").addEventListener("click", volverAtras);
                                                                                 //
                                                                                 //
///////////////////////////////////////////////////////////////////////////////////

}