let urlParams = getUrlVars();

fetch("../api/v1.0/single-parcela-info?parcel_id=" + urlParams.parcel_id).then(r => {
    return r.json();
}).then(res => {
    printHTML(res);
})


function printHTML(data){
    let ctx = `<option value="0">Todos los dispositivos</option>`;
    if(data.dispositivos.length > 0){
        data.dispositivos.forEach(device => {
            ctx += `<option value=${device.id}>
                            Dispositivo ${device.mac}
                            </option>
                        `
        });
        document.getElementById("seleccionar-dispositivo").innerHTML = ctx;
    }
    data.dispositivos.forEach(device => {
        var marker = new google.maps.Marker({
            position: {lat: device.lat, lng: device.lng},
            label: device.id.toString(),
            animation: google.maps.Animation.DROP,
            map: map
        });
    });
    let bounds = new google.maps.LatLngBounds();
                
                let polygon = new google.maps.Polygon({
                    paths: data.vertices,
                    strokeColor: "orange",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "orange",
                    fillOpacity: 0.35,
                    map: map
                });
                polygon.getPath().getArray().forEach( v => bounds.extend(v));
                map.fitBounds(bounds);
}








/**
 * Con esta función obtenemos todas las variables de la URL
 * 
 * PRO TIP:
 *      Si queremos obtener solo un elemento en concreto, esto seria de la sigueinte forma:
 *          let parcel_id = getUrlVars()['parcel_id];
 * 
 * En el anterior caso dentro de la variable creada tendremos solo el valor de dicho parametro en caso de que exista
 */

 
function getUrlVars() {
    let vars = {};
    let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m,key,value) => {
        vars[key] = value;
    });
    return vars;
}