
let ModeloParcelas = {
    cargar : function () {
        fetch("../api/v1.0/modelos/get-parcelas.php").then(function (respuesta) {
            return respuesta.json();
        }).then((json) => {
            this.datos = json;
            this.controlador.representar(this.datos);
        })
    },
    datos: [],
    controlador: {}
};

let VistaSelectorParcelas = {
    selector: {},
    preparar: function(selectId) {
        this.selector = document.getElementById(selectId);
        this.selector.innerHTML = "<option value='0'>Todos</option>";
    },
    representar : function (datos) {
        datos.forEach((parcels) => {
            this.selector.innerHTML += `<option value="${parcels.id}">
            ${parcels.id}, ${parcels.name}
            </option>`;
        })
    }
};

let ControladorParcelas = {
    modelo : ModeloParcelas,
    vista: VistaSelectorParcelas,
    iniciar : function() {
        this.modelo.controlador = this;
        this.modelo.cargar();
    },
    representar: function (datos) {
        this.vista.representar(datos);
    }
};