// Obtener los datos del servidor
fetch('datos/temperatura.json').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosTemperatura(j);
   });
   function procesarDatosTemperatura(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // consoleconsole.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        }) 
            // console.log(fechas);
            // console.log(totales);
            datos1.labels = fechas;
            datos1.datasets[0].data = totales;

            CrearGraficaTemperatura();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('datos/humedad.json').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosHumedad(j);
   });
   function procesarDatosHumedad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        }) 
            // console.log(fechas);
            // console.log(totales);
            datos2.labels = fechas;
            datos2.datasets[0].data = totales;

            CrearGraficaHumedad();
   }  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('datos/luminosidad.json').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosLuminosidad(j);
   });
   function procesarDatosLuminosidad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        }) 
            // console.log(fechas);
            // console.log(totales);
            datos3.labels = fechas;
            datos3.datasets[0].data = totales;

            CrearGraficaLuminosidad();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('datos/salinidad.json').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosSalinidad(j);
   });
   function procesarDatosSalinidad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        }) 
            // console.log(fechas);
            // console.log(totales);
            datos4.labels = fechas;
            datos4.datasets[0].data = totales;

            CrearGraficaSalinidad();
   }
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: ["2020-03-29","2020-03-30","2020-03-31"],
    datasets: [{ 
        data: [43,32,10],
        label: "Salinidad",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [34,12,56],
        label: "Humedad",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [50,55,60],
        label: "Luminosidad",
        borderColor: "#FF9300",
        fill: false
      }, { 
        data: [20,21,11],
        label: "Temperatura",
        borderColor: "#253016",
        fill: false
      }
    ]
  },
  options: {
    legend: {
      display: false
    },
    title: {
      display: false,
    }
  }
});
Chart.plugins.register({
  beforeDraw: function(chartInstance, easing) {
    var ctx = chartInstance.chart.ctx;
    ctx.fillStyle = '#FFF2D2'; // your color here

    var chartArea = chartInstance.chartArea;
    ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
  }
});

