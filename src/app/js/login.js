function login(ev){
    ev.preventDefault();

    fetch("./api/v1.0/sesion",{
                                method: "POST",
                                body: new FormData(document.getElementById("loginForm"))

    }).then( res => {
        if(res.status == 200){
            res.json().then(user => {
                if(user.role === "admin" || user.role === "user"){
                    location.href = "./app/list-parcels.html";
                }else{
                    location.href = "./app/list-enterprises.html";
                }
            })
        }else{
            document.getElementById("loginError").innerHTML = "Usuario o contraseña incorrecto";
            document.getElementById("loginError").style.color = "red";
        }
    })

}