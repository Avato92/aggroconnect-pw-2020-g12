// Obtener los datos del servidor
fetch('../api/v1.0/temperature').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosTemperatura(j);
   });
   function procesarDatosTemperatura(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // consoleconsole.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        })
            // console.log(fechas);
            // console.log(totales);
            datos1.labels = fechas;
            datos1.datasets[0].data = totales;

            CrearGraficaTemperatura();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('../api/v1.0/humidity').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosHumedad(j);
   });
   function procesarDatosHumedad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        })
            // console.log(fechas);
            // console.log(totales);
            datos2.labels = fechas;
            datos2.datasets[0].data = totales;

            CrearGraficaHumedad();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('../api/v1.0/luminosity').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosLuminosidad(j);
   });
   function procesarDatosLuminosidad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        })
            // console.log(fechas);
            // console.log(totales);
            datos3.labels = fechas;
            datos3.datasets[0].data = totales;

            CrearGraficaLuminosidad();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch('../api/v1.0/salinity').then(function (r) {
    return r.json();
   }).then(function (j) {
    // procesar los datos
    procesarDatosSalinidad(j);
   });
   function procesarDatosSalinidad(datosSensor) { //oredena las fechas
        datosSensor = datosSensor.sort(function (a, b) {
        if (a.fecha < b.fecha) return -1;
        if (a.fecha > b.fecha) return 1;
        return 0;
        });
    // console.log(datosSensor);
    // recorrer las ventas 0:55
    let fechas = [];
    let totales = [];
    datosSensor.forEach(function (dato) {
        let i = fechas.indexOf(dato.fecha);
        if(i < 0) {
            fechas.push(dato.fecha);
            totales.push(parseFloat(dato.valor));
            } else {
            totales[i] += parseFloat(dato.valor);
            }
        })
            // console.log(fechas);
            // console.log(totales);
            datos4.labels = fechas;
            datos4.datasets[0].data = totales;

            CrearGraficaSalinidad();
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Config tabla de temperatura
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let datos1 = {labels: [],
    datasets: [{
    label: 'Temperatura en °C',
    data: [],
    fill:false,//campo que se pinta bajo la linea
    backgroundColor: 'rgb(37, 48, 22)',
    borderColor: 'rgb(37, 48, 22)',
    borderDash: [2, 3],
    lineTension: 0,
    pointStyle: 'rectRot',
    pointRadius: 7,
    }]
   };

   let opciones1 = {
    maintainAspectRatio:false,
    title: {
    display: false,
    text: 'Temperatura'
    },
    tooltips: {
    backgroundColor: '#fff',
    titleFontColor: '#000',
    titleAlign: 'center',
    bodyFontColor: '#333',
    borderColor: '#666',
    borderWidth: 1,
    }
   };
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Config tabla de humedad
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let datos2 = {labels: [],
    datasets: [{
    label: 'Humedad en %',
    data: [],
    fill:false,//campo que se pinta bajo la linea
    backgroundColor: 'rgb(37, 48, 22)',
    borderColor: 'rgb(37, 48, 22)',
    borderDash: [2, 3],
    lineTension: 0,
    pointStyle: 'rectRot',
    pointRadius: 7,
    }]
   };

   let opciones2 = {
    maintainAspectRatio:false,
    title: {
    display: false,
    text: 'Humedad'
    },
    tooltips: {
    backgroundColor: '#fff',
    titleFontColor: '#000',
    titleAlign: 'center',
    bodyFontColor: '#333',
    borderColor: '#666',
    borderWidth: 1,
    }
   };
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Config tabla de luminosidad
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let datos3 = {labels: [],
    datasets: [{
    label: 'Intensidad luminosa',
    data: [],
    fill:false,//campo que se pinta bajo la linea
    backgroundColor: 'rgb(37, 48, 22)',
    borderColor: 'rgb(37, 48, 22)',
    borderDash: [2, 3],
    lineTension: 0,
    pointStyle: 'rectRot',
    pointRadius: 7,
    }]
   };

    let opciones3 = {
    maintainAspectRatio:false,
    title: {
    display: false,
    text: 'Luminosidad'
    },
    tooltips: {
    backgroundColor: '#fff',
    titleFontColor: '#000',
    titleAlign: 'center',
    bodyFontColor: '#333',
    borderColor: '#666',
    borderWidth: 1,
    }
   };
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Config tabla de salinidad
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let datos4 = {labels: [],
    datasets: [{
    label: 'Salinidad en %',
    data: [],
    fill:false,//campo que se pinta bajo la linea
    backgroundColor: 'rgb(37, 48, 22)',
    borderColor: 'rgb(37, 48, 22)',
    borderDash: [2, 3],
    lineTension: 0,
    pointStyle: 'rectRot',
    pointRadius: 7,
    }]
   };

    let opciones4 = {
    maintainAspectRatio:false,
    title: {
    display: false,
    text: 'Luminosidad'
    },
    tooltips: {
    backgroundColor: '#fff',
    titleFontColor: '#000',
    titleAlign: 'center',
    bodyFontColor: '#333',
    borderColor: '#666',
    borderWidth: 1,
    }
   };
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function CrearGraficaTemperatura() {
    let ctx = document.getElementById('chart-temperatura');
    let miGrafica= new Chart(ctx, {
    type: 'line',
    data: datos1,
    options: opciones1
    });

   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function CrearGraficaHumedad() {
    let ctx1 = document.getElementById('chart-humedad');
    let miGrafica1 = new Chart(ctx1, {
    type: 'line',
    data: datos2,
    options: opciones2
    });
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function CrearGraficaLuminosidad(){
    let ctx2 = document.getElementById('chart-luminosidad');
    let miGrafica2 = new Chart(ctx2, {
    type: 'line',
    data: datos3,
    options: opciones3
    });
   }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function CrearGraficaSalinidad(){
    let ctx3 = document.getElementById('chart-salinidad');
    let miGrafica3 = new Chart(ctx3, {
    type: 'line',
    data: datos4,
    options: opciones4
    });
   }
