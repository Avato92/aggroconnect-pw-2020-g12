let ModelEnterprises = {
    url: '../api/v1.0/enterprises',
    load: function(url = ''){
        if(url === ''){
            url = this.url;
        }
        fetch(url).then(res => {
            return res.json();
        }).then(enterprises => {
            console.log(enterprises);
            this.enterprises = enterprises;
            this.controller.print(this.enterprises);
        })
    },
    enterprises: [],
    controller: {}
};

let ViewEnterprises = {
    element: {},
    make: elementID => {
        this.element = document.getElementById(elementID);
    },
    print: function(enterprises){
        this.element.innerHTML = "";
    }
}

let ControllerEnterprises = {
    model: ModelEnterprises,
    view: ViewEnterprises,
    start: function() {
        this.model.controller = this;
        this.model.load();
    },
    print: function(enterprises){
        this.view.print(enterprises);
    }
}