fetch('../api/v1.0/parcelas', {
                                method: "GET"
                            }
                        ).then(res => {
                                return res.json();
                        }
                    ).then( parcels => {
                        printParcels(parcels);
                        
                    });

function printParcels(parcels){
    /**
    * TODO
    * Falta comprobar rol del usuario para pintar los botones que sean necesarios
    */

    if(parcels.length > 0){
        /**
        * Pintar las tarjetas
        */
        let ctx = `
          <div class="container mt-3 ">
              <fieldset class="list_container">
                <legend class="list_container__title">
                  <h3>Tus parcelas</h3></legend>
                  <input  type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar por nombres de parcelas.."/>
                  <div id="container">
                  
                
        `

        parcels.forEach(element => {

            /**
             * 
             * Tarjeta USER
             * 
             */

            ctx += `
            <section class="box">
            <div class="ralla-superior border-top">
            <div class="row">
              <div class="col d-lg-none "><div class=" mt-2"> ${element.name}</div></div>
              <div class="col-2 d-none d-lg-block" ><div class=" mt-2 ">${element.name}</div></div>
              <div class="col d-lg-none "><div class=" mt-2 ml-3">Media de las últimas mediciones</div></div>
              <div class="col-3 d-none d-lg-block" ></div>
              <div class="col d-none d-lg-block" ><div class=" mt-2 ml-3">Media de las últimas mediciones</div></div>
              <div class="col d-none d-lg-block" ></div>
            </div>

            <div class="row" id="fila">
                
                <div class="col d-lg-none "><div class=" mt-4"><a href="./parcel-data.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
                <div class="col d-lg-none" ><div class=" mt-4"><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 %</div></div>
                <div class="col-2 d-none d-lg-block" ></div>
                <div class="col-3 d-none d-lg-block" ></div>
                <div class="col-4 d-none d-lg-block" ><div class=" mt-4 "><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 % <img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div></div>
                <div class=" col-2 col d-none d-lg-block mb-3" ><div class=" mt-3 mt-4 "><a href="./parcel-data.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
            </div>          
              <div class="row" id="fila">
                <div class="col d-none d-lg-block" ></div>
              <div class="col d-none d-lg-block" ></div>
              <div class="col d-lg-none" ></div>
              <div class="col d-lg-none " ><div class=" mt-0" ><img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div>
              </div>
              </div>
              </section>
              
            `


            /**
             * 
             * Tarjeta ROOT/SAT
             * <div class="container mt-3 ">
                <fieldset class="list_container">
                <legend class="list_container__title">
                <div class="edit"><h3>Tus parcelas</h3><a href="./put-parcela-form.html" class="btn" id="edit">Crear parcelas</a></div></legend>
                <div class="container__cards" >
                    <div class="ralla-superior border-top mt-3">

                        <div class="row">
                          <div class="col d-lg-none "><div class=" mt-2"> ${element.name}</div></div>
                          <div class="col-2 d-none d-lg-block" ><div class=" mt-2 ">${element.name}</div></div>
                          <div class="col d-lg-none "><div class=" mt-2 ml-3">Últimas mediciones</div></div>
       
                          <div class="col d-none d-lg-block" ><div class=" mt-2 ml-3">Últimas mediciones</div></div>
                          <div class="col d-none d-lg-block" ></div>
                        </div>
        
                        <div class="row" id="fila">
                            
                            <div class="col d-lg-none "><div class=" mt-4"><a href="./parcel-map-bootstrap.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
                            <div class="col d-lg-none" ><div class=" mt-4"><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 %</div></div>
                            <div class="col-2 d-none d-lg-block" ></div>
                            <div class="col-4 d-none d-lg-block" ><div class=" mt-4 "><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 % <img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div></div>
                            <div class=" col-2 col d-none d-lg-block" ><div class=" mt-3 mt-4 "><a href="./parcel-map-bootstrap.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
                            <div class="col-2 d-none d-lg-block" ><div class=" mt-4 "><a href="./#" class="btn">Editar parcelas</a></div></div>
                            <div class="col-2 d-none d-lg-block" ><div class=" mt-4 "><a href="./#" class="btn" id="red">Eliminar parcelas</a></div></div>
                        </div>
        
                          <div class="row" id="fila">
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-lg-none"><div class=" mt-2"><a href="./#" class="btn">Editar parcelas</a></div></div>
                            <div class="col d-lg-none " ><div class=" mt-0" ><img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div></div>
                          </div>
        
                          <div class="row" id="fila">
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-lg-none"><div class=" mt-2"><a href="./#" class="btn" id="red">Eliminar parcelas</a></div></div>
                            
                            
                          </div>
        
                        </div>
             *  
             */


             /**
              * 
              * Tarjeta ADMIN
              * 
  <div class="ralla-superior border-top mt-3">

                        <div class="row">
                          <div class="col d-lg-none "><div class=" mt-2"> ${element.name}</div></div>
                          <div class="col-2 d-none d-lg-block" ><div class=" mt-2 ">${element.name}</div></div>
                          <div class="col d-lg-none "><div class=" mt-2 ml-3">Últimas mediciones</div></div>
                          <div class="col-1 d-none d-lg-block" ></div>
                          <div class="col d-none d-lg-block" ><div class=" mt-2 ml-3">Últimas mediciones</div></div>
                          <div class="col d-none d-lg-block" ></div>
                        </div>
        
                        <div class="row" id="fila">
                            
                            <div class="col d-lg-none "><div class=" mt-4"><a href="./parcel-map-bootstrap.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
                            <div class="col d-lg-none" ><div class=" mt-4"><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 %</div></div>
                            <div class="col-2 d-none d-lg-block" ></div>
                            <div class="col-1 d-none d-lg-block" ></div>
                            <div class="col-4 d-none d-lg-block" ><div class=" mt-4 "><img class="img" src="./style/images/thermometer-quarter-solid.svg ">10ºC <img class="img" src="./style/images/tint-solid.svg "> 20 % <img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div></div>
                            <div class=" col-2 col d-none d-lg-block" ><div class=" mt-3 mt-4 "><a href="./parcel-map-bootstrap.html?parcel_id=${element.id}" class="btn">Panel de seguimiento</a></div></div>
                            <div class="col-2 d-none d-lg-block" ><div class=" mt-4 "><a href="./#" class="btn">Editar parcelas</a></div></div>
                        </div>
        
                          <div class="row" id="fila">
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-lg-none"><div class=" mt-2"><a href="./#" class="btn">Editar parcelas</a></div></div>
                            <div class="col d-lg-none " ><div class=" mt-1" ><img class="img" src="./style/images/sun-solid.svg ">20 % <img class="img" src="./style/images/eye-dropper-solid.svg "> 20 %</div></div>
                          </div>
        
                          <div class="row" id="fila">
                            <div class="col d-none d-lg-block" ></div>
                            <div class="col d-none d-lg-block" ></div>
                            
                            
                          </div>
        
                        </div>
              */
            });
            ctx += `

            </div>
            </fieldset>
            </div>
         
         
            `
            document.getElementById("parcels_section").innerHTML = ctx;
    }else{

    /**
    * Pintar no hay parcelas
    */

        let ctx = `
                    <fieldset class="list_container">
                        <legend class="list_container__title"><h2>Ohh vaya!</h2></legend>
                        <div class="container__empty" >
                            <p>Al parecer no tienes parcelas asignadas</p>
                            <p>Esto es posible que sea debido a que nuestro equipo de asistencia técnica aún está haciendo trabajos,
                            por favor tengo paciencia</p>
                            <p>En caso de que el problema persista o que ya haya accedido a la aplicación anteriormente, puede ponerse 
                            en contacto con nosotros con una de las siguientes formas:</p>
                            <div>
                                <img alt="icono teléfono de contacto" src="./style/images/telefono-color.png">
                                <strong>Teléfono: 666 66 66 66</strong>
                            </div>
                            <div>
                                <img alt="icono correo electrónico" src="./style/images/correo-color.png">
                                <strong>Email: gti@upv.es</strong>
                            </div>
                        </div>
                    </fieldset>
                `       
        document.getElementById("parcels_section").innerHTML = ctx;

    }
}

